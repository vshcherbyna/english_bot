import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TasksModule } from './tasks/tasks.module';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from './users/users.module';
import { BucketUploadModule } from './bucket-upload/bucket-upload.module';
import { ImagesModule } from './images/images.module';
import { ProfilesModule } from './profiles/profiles.module';
import { CheckerModule } from './checker/checker.module';
import { QuizzesModule } from './quizzes/quizzes.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://mongo:27017'),
    TasksModule,
    UsersModule,
    BucketUploadModule,
    ImagesModule,
    ProfilesModule,
    CheckerModule,
    QuizzesModule,
    AuthModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

import { PassportStrategy } from '@nestjs/passport';
import { BasicStrategy } from 'passport-http';
import { Injectable, UnauthorizedException } from '@nestjs/common';

@Injectable()
export class BasicAuthStrategy extends PassportStrategy(BasicStrategy, 'basic-strategy') {
  constructor() {
    super();
  }

  validate(username: string, password: string): any {
    if (
      !(

        username === 'english' &&
        password === 'English2303'
      )
    ) {
      throw new UnauthorizedException();
    }
    return { username };
  }
}

import { Body, Controller, Param, Post, Res } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthDto } from './dto/auth.dto';
import { Auth } from './interfaces/auth.interface';
import { ApiBasicAuth } from '@nestjs/swagger';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService
  ) {}

  @Post('/login')
  async checkAuth(@Res() res, @Body() authData: AuthDto ): Promise<any> {
    const authInfo: Auth = await this.authService.checkUser(authData);
    res.status(200).json(authInfo);
  }
}


export interface Auth {
  readonly username: string;
  readonly password: string;
}

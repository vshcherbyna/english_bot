
export class UpdateUserDto {
  readonly email?: string;
  readonly points?: number;
  readonly blocked?: boolean;
  readonly missedDaysCount?: number;
}

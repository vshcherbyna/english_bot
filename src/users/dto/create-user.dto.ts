import {ApiModelProperty} from "@nestjs/swagger/dist/decorators/api-model-property.decorator";
import { IsNotEmpty, IsEmpty, IsOptional, IsString, IsNumber} from 'class-validator';
import { Type } from 'class-transformer';

export class CreateUserDto {
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    readonly email: string;

    @ApiModelProperty()
    @IsNumber()
    @IsOptional()
    readonly points: number;
}

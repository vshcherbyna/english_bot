import { Body, Controller, Delete, Get, Param, Patch, Post, Res, UseGuards } from '@nestjs/common';
import { TasksService } from '../tasks/tasks.service';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { ApiBasicAuth, ApiResponse } from '@nestjs/swagger';
import { User } from './interfaces/user.interface';
import { AuthGuard } from '@nestjs/passport';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectModel } from '@nestjs/mongoose';

@UseGuards(AuthGuard('basic-strategy'))
@Controller('users')
@ApiBasicAuth()
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    @InjectModel('TaskComplete') private readonly taskCompleteModel,
  ) {}

  @ApiResponse({ status: 204, description: 'The user has been successfully created'})
  @Post()
  create(@Body() createUserDto: CreateUserDto): Promise<any> {
    return this.usersService.create(createUserDto);
  }

  @Delete()
  async deleteAll(): Promise<any> {
    await this.taskCompleteModel.remove({});
    return this.usersService.deleteAll();
  }

  @Get()
  getAll(): Promise<User[]> {
    return this.usersService.findAll();
  }

  @Get('/emails/:email')
  async getByName(@Param('email') email: string, @Res() res): Promise<User> {
    const user = await this.usersService.getByEmail(email);
    return res.status(200).json(user);
  }

  @Delete('/emails/:email')
  async deleteByName(@Param('email') email: string, @Res() res): Promise<User> {
    const user = await this.usersService.deleteByName(email);
    return res.status(204).json();
  }

  @Post('/days/:day/blocked-job')
  async runBlockUsersJob(@Param('day') day: number, @Res() res): Promise<any> {
    await this.usersService.blockUsersJob(day);
    return res.status(204).json();
  }

  @Patch('/emails/:email')
  async updateUser(@Param('email') email: string, @Body() data: UpdateUserDto, @Res() res): Promise<any> {
    await this.usersService.update(email, data) ;
    return res.status(204).json();
  }

  @Post('/days/:day/extra-points')
  async extraPoints(@Param('day') day: number, @Res() res): Promise<any> {
    await this.usersService.addExtraPointsFirstUser(day);
    return res.status(204).json();
  }


}

import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './interfaces/user.interface';
import { Model } from 'mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { TaskComplete } from '../checker/interfaces/task-complete.interface';
import { CheckerService } from '../checker/checker.service';

const MAX_MISSED_DAYS = 2;

@Injectable()
export class UsersService {
  private logger: Logger;

  constructor(
    @InjectModel('User') private readonly userModel: Model<User>,
    @InjectModel('TaskComplete') private readonly taskCompleteModel: Model<TaskComplete>,
    private readonly checkerService: CheckerService
  ) {
    this.logger = new Logger(UsersService.name);
  }

  async deleteAll() {
    return await await this.userModel.remove({});
  }

  create(data: CreateUserDto): Promise<User> {
    let user = new this.userModel(data);
    return user.save();
  }

  async getByEmail(email: string): Promise<User> {
    const user = await this.userModel.findOne({email});
    if(!user) {
      throw new NotFoundException(`User ${email} doesn't exist`)
    }
    user.points = parseFloat(user.points.toFixed(1));
    if(!user) {
      throw new NotFoundException(`User with email: ${email} not found`);
    }
    return user;
  }

  async addPoints(email: string, points: number, day: number): Promise<boolean> {
    let user: User = await this.userModel.findOne({email});
    let blocked: boolean = false;
    let missedDaysCount: number = 0;
    if (user) {
      points = user.points + points;
      blocked = user.blocked;
      missedDaysCount = user.missedDaysCount;
    } else {
      missedDaysCount = day - 1;
      if (missedDaysCount >= 2) {
        blocked = true;
      }
    }
    await this.userModel.findOneAndUpdate({ email }, {
      $set: { email, points, blocked, missedDaysCount}
    }, {upsert: true});
    return true;
  }

  async findAll(): Promise<User[]> {
    return this.userModel.find({});
  }

  async update(email: string, user: UpdateUserDto): Promise<User> {
    return await this.userModel.findOneAndUpdate({ email }, {
      $set: user
    }, {upsert: true});
  }

  async deleteByName(email: string) {
    await this.taskCompleteModel.remove({email});
    return await this.userModel.remove({email});
  }

  async blockUsersJob(day: number) {
    const users: User[] = await this.findAll();
    for (let user of users) {
      let taskCompleteInfo = await this.taskCompleteModel.findOne({day, email: user.email});
      if (!taskCompleteInfo) {
        if (user.missedDaysCount > MAX_MISSED_DAYS) {
          await this.update(user.email, {
            blocked: true,
            missedDaysCount: user.missedDaysCount + 1
          });
        } else {
          await this.update(user.email, {
            missedDaysCount: user.missedDaysCount + 1
          });
        }
      }
      if (user.missedDaysCount > MAX_MISSED_DAYS) {
        await this.update(user.email, {
          blocked: true,
          missedDaysCount: user.missedDaysCount + 1
        });
      }
    }
  }

  async addExtraPointsFirstUser(day) {
    const userData = await this.checkerService.setExtraPointsToFirstUser(day);
    if (userData) {
      await this.addPoints(userData.email, userData.points, day);
    }
  }

}

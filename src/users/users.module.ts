import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { TaskSchema } from '../tasks/schemas/task.schema';
import { UserSchema } from './schemas/user.schema';
import { CheckerModule } from '../checker/checker.module';

@Module({
  providers: [UsersService],
  controllers: [UsersController],
  imports: [MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
    CheckerModule],
  exports: [UsersService]
})
export class UsersModule {}

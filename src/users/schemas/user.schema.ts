import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema({
  email: {type: String, required: true},
  points: {type: Number, required: true, default: 0},
  blocked: {type: Boolean, required: true, default: false},
  missedDaysCount: {type: Number, required: true, default: 0}
});

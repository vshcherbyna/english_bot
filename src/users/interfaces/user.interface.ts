import { Document } from 'mongoose';

export interface User extends Document {
  readonly email: string;
  readonly points: number;
  readonly blocked: boolean;
  readonly missedDaysCount: number;
}

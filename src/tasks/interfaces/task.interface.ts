import { Document } from 'mongoose';
import { Question} from './question.interface';
import { Choice} from './choice.interface';

export interface Task extends Document {
  readonly _id: string;
  readonly day: number;
  readonly mainQuestion: string;
  readonly type: string;
  readonly questions: Question[],
  readonly choices: Choice[],
  readonly title: string;
  readonly imageAnswers?: string[];
}

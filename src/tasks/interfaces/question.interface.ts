
export interface Question {
  readonly _id?: string;
  readonly question: string;
  readonly answers: string[];
  readonly choices: string[];
  readonly index: number;
}

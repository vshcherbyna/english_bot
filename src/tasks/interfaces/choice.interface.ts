
export interface Choice {
  readonly choice: string;
  readonly text: string;
}

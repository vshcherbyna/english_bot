import * as mongoose from 'mongoose';

export const TaskSchema = new mongoose.Schema({
  day: {type: Number, required: true},
  mainQuestion: {type: String, required: true},
  type: {
    type: String,
    required: true,
    enum: ['TEXT', 'IMAGE', 'LIST', 'IMAGE_LIST', 'CHOICE_LIST']
  },
  questions: [
    {
      question: {type: String, required: true},
      answers: [
        {type: String, required: true}
      ],
      choices: [
        {type: String, required: false}
      ],
      index: {type: Number, required: false},
    }
  ],
  imageAnswers: {type: Array, required: false},
  choices: [
    {
      choice: {type: String, required: false},
      text: {type: String, required: false}
    }
  ],
  title: {type: String, required: false},
  answerData: {type: String, required: false},
});

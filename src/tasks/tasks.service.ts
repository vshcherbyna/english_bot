import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { Task } from './interfaces/task.interface';
import { CreateTaskDto } from './dto/create-task.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { TasksEnum } from './tasks.enum';
import { UsersService } from '../users/users.service';
import { CheckerService } from '../checker/checker.service';
import { TaskComplete } from '../checker/interfaces/task-complete.interface';
import { User } from '../users/interfaces/user.interface';
import * as util from 'util';

import { UpdateTaskDto } from './dto/update-task.dto';
import { UpdateTaskCompleteDto } from './dto/update-task-comlete.dto';

@Injectable()
export class TasksService {
  private logger: Logger;

  constructor(
    @InjectModel('Task') private readonly taskModel: Model<Task>,
    @InjectModel('TaskComplete') private readonly taskCompleteModel: Model<TaskComplete>,
    private readonly usersService: UsersService,
    private readonly checkerService: CheckerService
  ) {
    this.logger = new Logger(TasksService.name);
  }

  async getAll(): Promise<Task[]>{
    return await this.taskModel.find();
  }

  async deleteAll() {
    return await this.taskModel.remove({});
  }

  async update(updateTaskDto: UpdateTaskDto, day) {
    return this.taskModel.findOneAndUpdate({ day: day }, {
      $set: updateTaskDto
    }, {upsert: true});
  }

  async getByDay(day: number): Promise<Task>{
    let task = await this.taskModel.findOne({
      day: day
    });
    if (!task) {
      throw new NotFoundException(`Task for day: ${day} not found`);
    }
    task = task.toObject();
    if (task.type === TasksEnum.MEDIA) {
      task.inputsCount = [...Array(task.inputsCount).keys()]
    }

    task.questions = task.questions.map(el => {
      return {
        question: el.question,
        choices: el.choices ? el.choices : [],
        _id: el._id
      }
    });
    return task;
  }

  create(task: CreateTaskDto, day: number): Promise<Task> {
    return this.taskModel.findOneAndUpdate({ day: day }, {
      $set: task
    }, {upsert: true});
  }

  async checkTask(day: number, userEmail: string, result: any) {
    let task = await this.taskModel.findOne({
      day: day
    });
    if (!task) {
      throw new NotFoundException(`Task for day: ${day} not found`);
    }
    let points = 0;
    if (task.type === TasksEnum.TEXT || task.type === TasksEnum.VIDEO) {
      points = await this.checkerService.textTaskProcess(day, userEmail, result.data, task);
    } else if (task.type === TasksEnum.CHOICE_LIST || task.type === TasksEnum.LIST || task.type === TasksEnum.LONG_LIST ||
      task.type === TasksEnum.MEDIA_CHOICE) {
      points = await this.checkerService.choiceListTaskProcess(day, userEmail, result.results, task);
    } else if (task.type === TasksEnum.IMAGE) {
      points = await this.checkerService.imageTaskProcess(day, userEmail, result.data, task);
    }  else if (task.type === TasksEnum.IMAGE_LIST || task.type === TasksEnum.IMAGE_CHOICE_LIST) {
      points = await this.checkerService.imageListTaskProcess(day, userEmail, result, task);
    } else if (task.type === TasksEnum.MEDIA) {
      points = await this.checkerService.choiceListTaskProcess(day, userEmail, result.results, task);
    }
    let taskComplete = await this.taskCompleteModel.findOne({ day, email: userEmail});

    if (points !== 0) {
      await this.checkerService.updateTaskCompleteObject(day, userEmail, {points: points});
      if (taskComplete && taskComplete.points && (task.type === TasksEnum.IMAGE_LIST || task.type === TasksEnum.IMAGE_CHOICE_LIST)) {
        console.log(taskComplete.points)
        points = points - taskComplete.points;
        console.log(points)
      }
    }

    await this.usersService.addPoints(userEmail, points, day);
  }

  async getUserStatistic(day: number, email: string): Promise<any> {
    const taskCompleteInfo = await this.taskCompleteModel.findOne({day, email});
    const taskInfo = await this.taskModel.findOne({day});
    return {
      questions: taskCompleteInfo ? taskCompleteInfo.questions : undefined,
      mainQuestion: taskInfo.mainQuestion,
      points: taskCompleteInfo ? taskCompleteInfo.points : 0
    };
  }

  async getUsersStatistic(day: number): Promise<any> {
    const taskInfo = await this.taskModel.findOne({day});
    const users: User[] = await this.usersService.findAll();
    const results = [];
    for (let user of users) {
      if (!user.blocked) {
        let taskCompleteInfo = await this.taskCompleteModel.findOne({ day, email: user.email });
        if (taskCompleteInfo) {
          taskCompleteInfo = this.updateTaskCompleteAnswers(taskCompleteInfo, taskInfo);
          results.push({
            email: user.email,
            day: day,
            dayPoints: taskCompleteInfo.points ? taskCompleteInfo.points.toFixed(2) : 0,
            totalPoints: user.points.toFixed(2),
            completed: true,
            questions: taskCompleteInfo.questions,
            mainQuestion: taskInfo.mainQuestion,
            imageAnswers: taskInfo.imageAnswers ? taskInfo.imageAnswers : [],
            date: taskCompleteInfo.date,
            checked: taskCompleteInfo.checked ? taskCompleteInfo.checked : false,
            first: taskCompleteInfo.first ? taskCompleteInfo.first : false
          });
        } else {
          results.push({
            email: user.email,
            day: day,
            dayPoints: 0,
            completed: false,
            totalPoints: user.points.toFixed(2),
            mainQuestion: taskInfo.mainQuestion,
            checked: false,
            first: false
          });
        }
      }
    }
    return results;
  }

  updateTaskCompleteAnswers(taskCompleteInfo, taskInfo: Task) {
    taskCompleteInfo = taskCompleteInfo.toObject();
    taskCompleteInfo.questions = taskCompleteInfo.questions.map(question => {
      question.userAnswers = question.userAnswers.join(', ');
      let taskQuestion = taskInfo.questions.find(el => {
        return el._id.toString() === question.questionId
      });
      if (taskQuestion) {
        question.questionAnswers = taskQuestion.answers.join(', ');
      } else {
        question.questionAnswers = question.questionAnswers.join(', ');
      }
      return question;
    });
    taskCompleteInfo.dayPoints = taskCompleteInfo.points;
    return taskCompleteInfo;
  }

  async updateTaskComplete(day: number, email: string, data: UpdateTaskCompleteDto) {
    const MAX_POINT = 3;
    if (!isNaN(data.points)) {
      let taskComplete = await this.taskCompleteModel.findOne({day, email});
      if (isNaN(taskComplete.points)) {
        taskComplete.points = 0;
      }
      let points: number = 0;
      if (!taskComplete.first) {
        points =  data.points - taskComplete.points;
      } else {
        points = (MAX_POINT + data.points) - taskComplete.points;
        data.points = data.points + MAX_POINT;
      }
      await this.usersService.addPoints(email, points, day)
    }
    await this.checkerService.updateTaskCompleteObject(day, email, data);
  }

  async updateTaskCompleteQuestionData(email: string, day: number, questionId: string, data: any) {
    return this.taskCompleteModel.findOneAndUpdate({day, email, questions: {
        $elemMatch: { _id: questionId}
      }},
      {
        $set: {
          'questions.$.correct': data.correct
        }
      });
  }

  async setFirstUserToTask(email: string, day: number) {

  }

}

import { Module } from '@nestjs/common';
import { TasksService } from './tasks.service';
import { TasksController } from './tasks.controller';
import { Task } from './tasks.entity';
import { MongooseModule } from '@nestjs/mongoose';
import { TaskSchema } from './schemas/task.schema';
import { UsersModule } from '../users/users.module';
import { CheckerModule } from '../checker/checker.module';
import { BasicAuthStrategy } from '../auth/basic-auth.strategy';

@Module({
  providers: [TasksService, BasicAuthStrategy],
  controllers: [TasksController],
  imports: [MongooseModule.forFeature([{ name: 'Task', schema: TaskSchema }]),
    UsersModule,
    CheckerModule]
})
export class TasksModule {}

import {
  Body,
  Controller,
  Get,
  Param, Patch,
  Post,
  Put,
  Query,
  Res,
  UseGuards,
  Delete,
} from '@nestjs/common';
import { ApiBasicAuth, ApiResponse } from '@nestjs/swagger';
import { TasksService } from './tasks.service';
import { CreateTaskDto } from './dto/create-task.dto';
import { AuthGuard } from '@nestjs/passport';

import { UpdateTaskDto } from './dto/update-task.dto';
import { UpdateTaskCompleteDto } from './dto/update-task-comlete.dto';


@UseGuards(AuthGuard('basic-strategy'))
@Controller('tasks')
@ApiBasicAuth()
export class TasksController {

  constructor(
    private readonly tasksService: TasksService
  ) {}

  @Get()
  findAll(): Promise<any> {
    return this.tasksService.getAll();
  }

  @Delete()
  deleteAll(): Promise<any> {
    return this.tasksService.deleteAll();
  }

  @Get('/days/:day')
  async findByDay(@Param('day') day: number, @Res() res): Promise<any> {
    const task = await this.tasksService.getByDay(day);
    res.status(200).json(task);
  }

  @ApiResponse({ status: 204, description: 'The task has been successfully created'})
  @Put('/days/:day')
  async createTask(@Param('day') day: number, @Res() res, @Body() createTaskDto: CreateTaskDto): Promise<any> {
    await this.tasksService.create(createTaskDto, day);
    res.status(204).send();
  }

  @ApiResponse({ status: 204, description: 'The task has been successfully updated'})
  @Patch('/days/:day')
  async updateTask(@Param('day') day: number, @Res() res, @Body() updateTaskDto: UpdateTaskDto): Promise<any> {
    await this.tasksService.update(updateTaskDto, day);
    res.status(204).send();
  }

  @Post('/days/:day/users/:userId/result')
  async checkTask(@Param('day') day: number, @Param('userId') userId: string,
                  @Res() res, @Body() result: any): Promise<any> {
                    console.log('----')
    await this.tasksService.checkTask(day, userId, result);
    res.status(204).send();
  }

  @Get('/days/:day/users/:userId/statistics')
  async getUserStatistic(@Param('day') day: number, @Param('userId') userId: string,
                         @Res() res): Promise<any> {
    const task = await this.tasksService.getUserStatistic(day, userId);
    res.status(200).json(task);
  }

  @Get('/days/:day/statistics')
  async getUsersStatistic(@Param('day') day: number,
                         @Res() res): Promise<any> {
    const task = await this.tasksService.getUsersStatistic(day);
    res.status(200).json(task);
  }

  @Put('/days/:day/users/:userId/taskComplete')
  async updateTaskComplete(@Param('day') day: number, @Param('userId') userId: string,
                           @Res() res, @Body() data: UpdateTaskCompleteDto): Promise<any> {
    await this.tasksService.updateTaskComplete(day, userId, data);
    res.status(204).json();
  }

  @Put('/days/:day/users/:userId/questions/:questionId/correct')
  async updateTaskCompleteQuestionComplete(@Param('day') day: number, @Param('userId') userId: string,
                                           @Param('questionId') questionId: string,
                           @Res() res, @Body() data: any): Promise<any> {
    await this.tasksService.updateTaskCompleteQuestionData(userId, day, questionId, data);
    res.status(204).json();
  }

}



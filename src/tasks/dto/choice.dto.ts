import { ApiModelProperty } from "@nestjs/swagger/dist/decorators/api-model-property.decorator";
import { IsNotEmpty } from 'class-validator';

export class ChoiceDto {
    @ApiModelProperty()
    @IsNotEmpty()
    readonly choice: string;

    @ApiModelProperty()
    @IsNotEmpty()
    readonly text: string;
}

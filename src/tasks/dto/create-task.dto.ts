import {ApiModelProperty} from "@nestjs/swagger/dist/decorators/api-model-property.decorator";
import { IsNotEmpty, IsEmpty, IsOptional, IsArray, ValidateNested, IsString, IsNumber } from 'class-validator';
import { QuestionDto } from './question.dto';
import { ChoiceDto } from './choice.dto';
import { Type } from 'class-transformer';

export class CreateTaskDto {
    @ApiModelProperty()
    @IsNotEmpty()
    readonly mainQuestion: string;

    @ApiModelProperty()
    @IsNotEmpty()
    readonly day: number;

    @ApiModelProperty()
    @IsNotEmpty()
    readonly type: string;

    @ApiModelProperty({type:  [QuestionDto]})
    @IsArray()
    @ValidateNested()
    @Type(() => QuestionDto)
    readonly questions: QuestionDto[];

    @ApiModelProperty({type: [ChoiceDto] })
    @IsArray()
    @IsOptional()
    @ValidateNested()
    @Type(() => ChoiceDto)
    readonly choices: ChoiceDto[];

    @ApiModelProperty({type: [ChoiceDto] })
    @IsArray()
    @IsOptional()
    readonly imageAnswers: string[];

    @ApiModelProperty()
    @IsString()
    @IsOptional()
    readonly title: string;

    @ApiModelProperty()
    @IsString()
    @IsOptional()
    readonly answerData: string;
}

import {ApiModelProperty} from "@nestjs/swagger/dist/decorators/api-model-property.decorator";
import { IsNotEmpty, IsOptional } from 'class-validator';

export class QuestionDto {
    @ApiModelProperty()
    @IsNotEmpty()
    readonly question: string;

    @ApiModelProperty()
    @IsNotEmpty()
    readonly answers: string[];

    @ApiModelProperty()
    @IsNotEmpty()
    readonly choices: string[];
}

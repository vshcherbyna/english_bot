import { ApiModelProperty } from "@nestjs/swagger/dist/decorators/api-model-property.decorator";
import { IsOptional } from 'class-validator';
import { CompletedQuestion } from '../../checker/interfaces/completed-question';

export class UpdateTaskCompleteDto {
  @ApiModelProperty()
  @IsOptional()
  points?: number;

  @ApiModelProperty()
  @IsOptional()
  readonly checked?: boolean;

  @ApiModelProperty()
  @IsOptional()
  readonly questions?: CompletedQuestion[];



}

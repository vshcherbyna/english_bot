
export enum TasksEnum {
  TEXT = 'text',
  IMAGE = 'image',
  LIST = 'list',
  IMAGE_LIST = 'imageList',
  CHOICE_LIST = 'choiceList',
  MEDIA = 'media',
  LONG_LIST = 'longList',
  VIDEO = 'video',
  IMAGE_CHOICE_LIST = 'imageChoiceList',
  MEDIA_CHOICE = 'mediaChoice'
}

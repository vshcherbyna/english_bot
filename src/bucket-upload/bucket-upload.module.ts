import { Module } from '@nestjs/common';
import { BucketUploadService } from './bucket-upload.service';

@Module({
  providers: [BucketUploadService],
  exports: [BucketUploadService]
})
export class BucketUploadModule {}


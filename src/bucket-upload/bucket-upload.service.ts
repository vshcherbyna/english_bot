'use strict';
import sharp from 'sharp';
import * as path from 'path';
import { Injectable, Logger } from '@nestjs/common';
import * as cloudinary from 'cloudinary';

cloudinary.v2.config({
  cloud_name: 'booka-herokuapp-com', //process.env.CLOUD_NAME,
  api_key: '287166916858793', //process.env.CLOUD_API_KEY,
  api_secret: '-LVPCQmUbsmKmzodih-fBzn1Rf8' //process.env.CLOUD_API_SECRET
});

/**
 * BucketUploadService upload files to s3 bucket
 */
@Injectable()
export class BucketUploadService {
  logger: Logger;

  /**
   * Constructs BucketUploadService
   */
  constructor() {
    this.logger = new Logger(BucketUploadService.name);
  }

  /**
   * Upload file to cloudinary
   * @param {String} fileName file name
   * @param {String} data file content
   */
  async uploadToBucket(fileName, data) {
    try {
      const image = await cloudinary.v2.uploader.upload(`data:image/png;base64,${data.toString('base64')}`);
      return {
        id: image.public_id,
        url: image.url
      };
    } catch (err) {
      this.logger.error(`Failed to save image ${fileName}`, err);
      throw err;
    }

  }

  async deleteImage(publicId) {
    try {
      await cloudinary.v2.uploader.destroy(publicId)
    } catch(err) {
      this.logger.error(`Failed to delete image ${publicId}`, err);
      throw err;
    }
  }

}

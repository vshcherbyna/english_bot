import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { Task } from '../tasks/interfaces/task.interface';
import { Question } from '../tasks/interfaces/question.interface';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CompletedQuestion } from './interfaces/completed-question';
import { TaskComplete } from './interfaces/task-complete.interface';
import { UpdateTaskCompleteDto } from '../tasks/dto/update-task-comlete.dto';

const MIN_POINT = 0;
const MID_POINT = 0;
const MAX_POINT = 3;

@Injectable()
export class CheckerService {

  constructor(
    @InjectModel('TaskComplete') private readonly taskCompleteModel: Model<TaskComplete>) {}

  public async imageListTaskProcess(day: number, email: string, result: any, task: Task) {
    let taskCompleteObj: TaskComplete = await this.taskCompleteModel.findOne({ day, email});
    let question: Question = task.questions.find(
      el => el._id.toString() === result.questionId);
    if (question) {

      if (taskCompleteObj && taskCompleteObj.questions.some(el => el.questionId === result.questionId)){
        return MIN_POINT;
      }
      const correct = question.answers[0].toLowerCase().trim() === result.data.toLowerCase().trim();
      console.log(correct)
      await this.updateTaskComplete(day, email, {
        questionId: question._id,
        completed: true,
        question: '',
        userAnswers: [result.data],
        questionAnswers: [question.answers[0]],
        correct: correct
      });
    }
    let allAnswersRightFlag = true;
    taskCompleteObj = await this.taskCompleteModel.findOne({ day, email});

    if (taskCompleteObj.questions.length === task.questions.length) {
      taskCompleteObj.questions.forEach(el => {
        if (!el.correct) {
          allAnswersRightFlag = false;
        }
      });
    } else {
      allAnswersRightFlag = false;
    }

    if (taskCompleteObj && allAnswersRightFlag) {
      return MAX_POINT;
    }
    return MID_POINT;
  }

  private async updateTaskComplete(day: number, email: string,
                                   completedQuestion: CompletedQuestion) {
    let taskCompleteObject = await this.taskCompleteModel.findOne({ day, email });
    let questions: CompletedQuestion[] = [];
    if (taskCompleteObject) {
      taskCompleteObject = taskCompleteObject.toObject();
      questions = taskCompleteObject.questions;
      if (!questions.some(el => el.questionId === completedQuestion.questionId)) {
        questions.push(completedQuestion);
      }
      return await this.taskCompleteModel.findOneAndUpdate({ day, email}, {
        $set: {questions}
      }, {upsert: true});
    } else {
      questions = [completedQuestion];
      return this.taskCompleteModel.findOneAndUpdate({ day, email}, {
        $set: {
          questions,
          date: Date.now()
        }
      }, {upsert: true});
    }
  }

  public async imageTaskProcess(day: number, email: string, result: string, task: Task): Promise<number> {
    let allAnswersRightFlag = true;
    let questionAnswers: string[] = task.questions[0].answers;

    let taskCompleteObj: TaskComplete = await this.taskCompleteModel.findOne({ day, email});
    if (taskCompleteObj) {
      return MIN_POINT;
    }

    questionAnswers = questionAnswers.map(el => el.trim().toLowerCase());
    let userAnswers: string[] = result.split(',');
    userAnswers = userAnswers.map(el => el.trim().toLowerCase());
    if (questionAnswers.length !== userAnswers.length) {
      allAnswersRightFlag = false;
    }
    userAnswers.forEach(answer => {
      if (!questionAnswers.some(el => el === answer)) {
        allAnswersRightFlag = false;
      }
    });

    await this.updateTaskComplete(day, email, {
      questionId: task.questions[0]._id,
      completed: true,
      question: '',
      userAnswers: userAnswers,
      questionAnswers: questionAnswers,
      correct: allAnswersRightFlag
    });
    if (allAnswersRightFlag) {
      return MAX_POINT;
    }
    return MID_POINT;
  }

  public async textTaskProcess(day: number, email: string, answer: string, task: Task): Promise<number> {
    let taskCompleteObj: TaskComplete = await this.taskCompleteModel.findOne({ day, email});
    if (taskCompleteObj) {
      return MIN_POINT;
    }
    if (answer) {
      let correct = true;
      let answerWords = answer.split(' ').map(el => el.toLowerCase().trim());
      task.questions[0].answers.forEach(questionAnswer => {
        answerWords.forEach(word => {
          if (!word.includes(questionAnswer)) {
            correct = false;
          }
        });
      });

      await this.updateTaskComplete(day, email, {
        questionId: task.questions[0]._id,
        completed: true,
        question: task.questions[0].question,
        userAnswers: [answer],
        questionAnswers: task.questions[0].answers,
        correct: correct
      });

      if (correct) {
        return MAX_POINT;
      }
    }
    return MID_POINT;
  }

  public async choiceListTaskProcess(day: number, email: string, results: any, task: Task): Promise<number> {
    console.log(results)
    let allAnswersRightFlag = true;
    if (results) {
      let taskCompleteObj: TaskComplete = await this.taskCompleteModel.findOne({ day, email});

      if (taskCompleteObj) {
        return MIN_POINT;
      }

      results = JSON.parse(results);
      for (let result of results) {
        const question = task.questions.find(el => {
          return el._id.toString() === result._id
        });

        let userAnswers: string[] = result.result.toLowerCase().split(', ');
        userAnswers = userAnswers.map(el => el.trim());
        let right: boolean = true;
        for ( let userAnswer of userAnswers) {
          console.log('-===+', userAnswer, question.answers);
          if (!question || !question.answers.some(el => el.toLowerCase().trim() === userAnswer)) {
            right = false;
          }
        }
        if (right) {
          await this.updateTaskComplete(day, email, {
            questionId: question._id,
            completed: true,
            question: question.question,
            userAnswers: [result.result],
            questionAnswers: [...question.answers],
            correct: true
          });
        } else {
          allAnswersRightFlag = false;
          await this.updateTaskComplete(day, email, {
            questionId: question._id,
            completed: true,
            question: question.question,
            userAnswers: [result.result],
            questionAnswers: [...question.answers],
            correct: false
          });
        }

      }
      if (allAnswersRightFlag) {
        return MAX_POINT;
      }
    }
    return MID_POINT;
  }

  async completeQuestion(day: number, email: string, questionId: string, task: Task) {
    let points = parseFloat(((MAX_POINT / task.questions.length)).toFixed(5));
    return await this.updateQuestionPoints(day, email, questionId, points);
  }

  async incompleteQuestion(day: number, email: string, questionId: string, task: Task) {
    return await this.updateQuestionPoints(day, email, questionId, MIN_POINT);
  }

  async updateQuestionPoints(day: number, email: string, questionId: string, points: number) {
    let taskCompleteInfo = await this.taskCompleteModel.findOne({ day, email });

    if (taskCompleteInfo) {
      taskCompleteInfo = taskCompleteInfo.toObject();
      let question = taskCompleteInfo.questions.find(el => el.questionId === questionId);

      if (question) {
        question.points = points;
        await this.taskCompleteModel.findOneAndUpdate({ day, email }, {
          $set: {questions: taskCompleteInfo.questions}
        });

        return taskCompleteInfo;
      }
    } else {
      throw new NotFoundException(`Task complete information for day: ${day}, user: ${email} not found`);
    }
  }

  public async updateTaskCompleteObject(day: number, email: string, data: UpdateTaskCompleteDto) {
    console.log(data)
    await this.taskCompleteModel.findOneAndUpdate({ day, email }, {
      $set: data
    });
  }

  async setExtraPointsToFirstUser(day: number) : Promise<any> {
    const taskComplete: TaskComplete = await this.taskCompleteModel.findOne({day, points: MAX_POINT}).sort({date: 1});
    if(!taskComplete.first) {
      await this.taskCompleteModel.findOneAndUpdate({ day: taskComplete.day, email: taskComplete.email }, {
        $set: {first: true, points: taskComplete.points + 1}
      });
      return {
        points: MAX_POINT,
        email: taskComplete.email
      };
    }
    return undefined;
  }

}

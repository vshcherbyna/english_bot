import * as mongoose from 'mongoose';

export const TaskCompleteSchema = new mongoose.Schema({
  day: {type: Number, required: true},
  email: {type: String, required: true},
  questions: [
    {
      question:  {type: String, required: false},
      questionId: {type: String, required: true},
      completed: {type: Boolean, required: true},
      userAnswers: [{type: String, required: true}],
      questionAnswers: [{type: String, required: true}],
      correct: {type: Boolean, required: true}
    }
  ],
  points: {type: Number, required: true},
  date: {type: Date, required: true, default: Date.now()},
  checked: {type: Boolean, required: true, default: false},
  first: {type: Boolean, required: true, default: false}
});

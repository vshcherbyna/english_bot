import { Document } from 'mongoose';
import { CompletedQuestion } from './completed-question';

export interface TaskComplete extends Document {
  readonly _id: number;
  readonly day: number,
  readonly email: string,
  questions: CompletedQuestion[],
  date: string,
  points: number,
  checked: boolean,
  first: boolean
}

import { Document } from 'mongoose';

export interface CompletedQuestion extends Document {
  readonly _id?: string,
  readonly question?: string,
  readonly questionId: string,
  readonly completed: boolean,
  readonly userAnswers: string[],
  readonly questionAnswers: string[],
  readonly correct: boolean
}

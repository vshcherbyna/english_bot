import { Module } from '@nestjs/common';
import { CheckerService } from './checker.service';
import { MongooseModule } from '@nestjs/mongoose';
import { TaskCompleteSchema } from './schemas/task-complete.schema';

@Module({
  providers: [CheckerService],
  imports: [
    MongooseModule.forFeature([{ name: 'TaskComplete', schema: TaskCompleteSchema }])
  ],
  exports: [
    CheckerService,
    MongooseModule.forFeature([{ name: 'TaskComplete', schema: TaskCompleteSchema }])
  ]
})
export class CheckerModule {}

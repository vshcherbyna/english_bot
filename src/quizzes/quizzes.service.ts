import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Quiz } from './interfaces/quiz.interface';
import { Model } from 'mongoose';
import { CreateQuizDto } from './dto/create-quiz.dto';

@Injectable()
export class QuizzesService {

  constructor( @InjectModel('Quiz') private readonly quizModel: Model<Quiz>) {}

  createOrUpdate(name: string, quiz: CreateQuizDto): Promise<Quiz> {
    return this.quizModel.findOneAndUpdate({ name: name }, {
      $set: quiz
    }, {upsert: true});
  }

  async getByName(name: string): Promise<Quiz> {
    const quiz: Quiz = await this.quizModel.findOne({ name });
    if (!quiz) {
      throw new NotFoundException(`Quiz with name: ${name} not found`);
    }
    return quiz;
  }

  async increaseDay(name: string): Promise<Quiz> {
    const quiz = await this.quizModel.findOne({ name });
    if (!quiz) {
      throw new NotFoundException(`Quiz with name: ${name} not found`);
    }
    return this.quizModel.findOneAndUpdate({ name: name }, {
      $set: {day: quiz.day + 1}
    });
  }
}

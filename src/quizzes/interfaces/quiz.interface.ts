import { Document } from 'mongoose';

export interface Quiz extends Document {
  readonly _id: string;
  readonly day: number;
  readonly name: string;
}

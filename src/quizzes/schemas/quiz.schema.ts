import * as mongoose from 'mongoose';

export const QuizSchema = new mongoose.Schema({
  day: {type: Number, required: true},
  name: {type: String, required: true, default: 0}
});

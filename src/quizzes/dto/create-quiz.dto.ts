import {ApiModelProperty} from "@nestjs/swagger/dist/decorators/api-model-property.decorator";
import { IsNotEmpty } from 'class-validator';

export class CreateQuizDto {
  @ApiModelProperty()
  @IsNotEmpty()
  readonly name: string;

  @ApiModelProperty()
  @IsNotEmpty()
  readonly day: number;

}

import { Body, Controller, Get, Param, Post, Put, Res, UseGuards } from '@nestjs/common';
import { ApiBasicAuth, ApiResponse } from '@nestjs/swagger';
import { QuizzesService } from './quizzes.service';
import { CreateQuizDto } from './dto/create-quiz.dto';
import { Quiz } from './interfaces/quiz.interface';
import { AuthGuard } from '@nestjs/passport';

@UseGuards(AuthGuard('basic-strategy'))
@Controller('quizzes')
@ApiBasicAuth()
export class QuizzesController {

  constructor(private readonly quizzesService: QuizzesService,) {
  }

  @ApiResponse({ status: 204, description: 'The task has been successfully created'})
  @Put('/quizzes/:name')
  async createQuiz(@Param('name') name: string, @Res() res,
                   @Body() createQuizDto: CreateQuizDto): Promise<any> {
    await this.quizzesService.createOrUpdate(name, createQuizDto);
    res.status(204).send();
  }

  @ApiResponse({ status: 204, description: 'The task has been successfully created'})
  @Put('/quizzes/:name')
  async getQuiz(@Param('name') name: string, @Res() res,
                   @Body() createQuizDto: CreateQuizDto): Promise<any> {
    await this.quizzesService.createOrUpdate(name, createQuizDto);
    res.status(204).send();
  }

  @ApiResponse({ status: 204, description: 'The task has been successfully created'})
  @Post('/quizzes/:name/days/increase')
  async increaseQuizDay(@Param('name') name: string, @Res() res): Promise<any> {
    await this.quizzesService.increaseDay(name);
    res.status(204).send();
  }

  @Get('/quizzes/:name')
  async findByDay(@Param('name') name: string, @Res() res): Promise<any> {
    const quiz: Quiz = await this.quizzesService.getByName(name);
    res.status(200).json(quiz);
  }
}

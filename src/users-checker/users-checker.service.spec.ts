import { Test, TestingModule } from '@nestjs/testing';
import { UsersCheckerService } from './users-checker.service';

describe('UsersCheckerService', () => {
  let service: UsersCheckerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UsersCheckerService],
    }).compile();

    service = module.get<UsersCheckerService>(UsersCheckerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

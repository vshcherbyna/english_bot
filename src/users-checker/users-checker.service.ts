import { Injectable } from '@nestjs/common';
import { User } from '../users/interfaces/user.interface';
import { UsersService } from '../users/users.service';
import { CheckerService } from '../checker/checker.service';
import { InjectModel } from '@nestjs/mongoose';
import { TaskComplete } from '../checker/interfaces/task-complete.interface';
import { Model } from 'mongoose';

const MAX_MISSED_DAYS = 2;

@Injectable()
export class UsersCheckerService {

  constructor(private readonly usersService: UsersService,
              private readonly checkerService: CheckerService,
              @InjectModel('TaskComplete') private readonly taskCompleteModel: Model<TaskComplete>) {
  }

  async blockUsersJob(day) {
    const users: User[] = await this.usersService.findAll();
    for (let user of users) {
      let taskCompleteInfo = await this.taskCompleteModel.findOne({day, email: user.email});
      if (!taskCompleteInfo) {
        if (user.missedDaysCount + 1 >= MAX_MISSED_DAYS) {
          await this.usersService.update(user.email, {
            blocked: true,
            missedDaysCount: user.missedDaysCount + 1
          });
        } else {
          await this.usersService.update(user.email, {
            missedDaysCount: user.missedDaysCount + 1
          });
        }
      }
    }
  }

}


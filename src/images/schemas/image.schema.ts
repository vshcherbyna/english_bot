import * as mongoose from 'mongoose';

export const ImageSchema = new mongoose.Schema({
  url: {type: String, required: true},
  publicId: {type: String, required: true},
  name: {type: String, required: true},
  day: {type: Number, required: true}
});

import { Body, Controller, Get, Param, Post, Res, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiBasicAuth, ApiBody, ApiConsumes } from '@nestjs/swagger';
import { FileInterceptor } from '@nestjs/platform-express';
import { ImagesService } from './images.service';
import { CreateImageDto } from './dto/create-image.dto';
import { Image } from './interfaces/image.interface';
import { AuthGuard } from '@nestjs/passport';

@UseGuards(AuthGuard('basic-strategy'))
@Controller('images')
@ApiBasicAuth()
export class ImagesController {

  constructor(
    private readonly imagesService: ImagesService,
  ) {}

  @Get()
  async findAll(): Promise<Image[]> {
    const images: Image[] = await this.imagesService.getAll();
    return images;
  }

  @Get('/days/:day')
  async findByDay(@Param('day') day: number): Promise<Image[]> {
    const images: Image[] = await this.imagesService.findByDay(day);
    return images;
  }

  @Post()
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FileInterceptor('file'))
  @ApiBody({
    type: CreateImageDto
  })

  async uploadImage(@UploadedFile() file, @Body() createImageDto: CreateImageDto, @Res() res): Promise<any> {
    await this.imagesService.uploadImage(file.buffer, createImageDto);
    res.status(204).send();
  }
}

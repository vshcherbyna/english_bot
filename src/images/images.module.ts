import { Module } from '@nestjs/common';
import { ImagesService } from './images.service';
import { ImagesController } from './images.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { BucketUploadModule } from '../bucket-upload/bucket-upload.module';
import { ImageSchema } from './schemas/image.schema';

@Module({
  providers: [ImagesService],
  controllers: [ImagesController],
  imports: [MongooseModule.forFeature([{ name: 'Image', schema: ImageSchema }]),
    BucketUploadModule]
})
export class ImagesModule {}

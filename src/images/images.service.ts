import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Task } from '../tasks/interfaces/task.interface';
import { BucketUploadService } from '../bucket-upload/bucket-upload.service';
import { Model } from 'mongoose';
import { Image } from './interfaces/image.interface';
import { CreateImageDto } from './dto/create-image.dto';

@Injectable()
export class ImagesService {
  private logger: Logger;

  constructor(
    @InjectModel('Image') private readonly imageModel: Model<Image>,
    private readonly bucketUploadService: BucketUploadService
  ) {
    this.logger = new Logger(ImagesService.name);
  }

  async getAll(): Promise<Image[]> {
    let images: Image[] = await this.imageModel.find();
    return images;
  }

  async findByDay(day: number): Promise<Image[]>  {
    let images: Image[] = await this.imageModel.find({day});
    return images;
  }

  async deleteImage(imageId: string) {
    try {
      await this.bucketUploadService.deleteImage(imageId);
    } catch (err) {
      throw new BadRequestException(err);
    }
  }

  async uploadImage(image: Buffer, imageData: CreateImageDto) {
    try {
      const imageInfo = await this.bucketUploadService.uploadToBucket('test.png', image);
      const imageObj = new this.imageModel({
        name: imageData.name,
        day: imageData.day,
        url: imageInfo.url,
        publicId: imageInfo.id
      });

      return imageObj.save();
    } catch (err) {
      throw new BadRequestException(err);
    }
  }
}

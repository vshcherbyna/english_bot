import { Document } from 'mongoose';

export interface Image extends Document {
  readonly publicId: string;
  readonly url: string;
  readonly name: string;
  readonly day: number;
}

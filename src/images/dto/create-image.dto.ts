import {ApiModelProperty} from "@nestjs/swagger/dist/decorators/api-model-property.decorator";
import { IsNotEmpty, IsNumber } from 'class-validator';
import { UploadedFile } from '@nestjs/common';

export class CreateImageDto {

  @ApiModelProperty({ type: 'string', format: 'binary' })
  file: any;

  @ApiModelProperty({ type: 'string'})
  @IsNotEmpty()
  readonly name: string;

  @ApiModelProperty({ type: Number})
  // @IsNumber()
  readonly day: number;

}

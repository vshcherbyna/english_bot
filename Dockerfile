FROM node:lts

WORKDIR /root/app
COPY package.json /root/app
RUN npm install
# Copy app
COPY . /root/app
EXPOSE 3000
CMD ["npm", "run", "start"]

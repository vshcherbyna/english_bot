import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import * as request from 'supertest';
import { UserModule } from '../../src/user/user.module';
import { UserService } from '../../src/user/user.service';
import { AppModule } from '../../src/app.module';
import { IUser } from '../../src/user/user.interface';

describe('Users', () => {
  function instanceOfUser(object: any): object is IUser {
    return 'id' in object && 'name' in object && 'surname' in object;
  }

  const expectedUser : IUser = {
    id: 'userId',
    name: 'name',
    surname: 'surname',
  };

  const userService = {
    getUsers: () => [expectedUser],
    getUserById: () => expectedUser,
    addUser: () => true,
  };

  let app: INestApplication;

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      imports: [AppModule, UserModule],
    })
      .overrideProvider(UserService)
      .useValue(userService)
      .compile();

    app = module.createNestApplication();
    await app.init();
  });

  it('/GET users', () => request(app.getHttpServer())
    .get('/users')
    .expect(200)
    .expect(userService.getUsers())
    .expect(({ body }) => {
      expect(Array.isArray(body)).toBeTruthy();
      body.forEach((user) => {
        expect(instanceOfUser(user)).toBeTruthy();
      });
    }));

  it('/GET users/:id', () => request(app.getHttpServer())
    .get('/users/test')
    .expect(200)
    .expect(userService.getUserById())
    .expect(({ body }) => {
      expect(instanceOfUser(body)).toBeTruthy();
    }));

  it('/POST users', () => request(app.getHttpServer())
    .post('/users')
    .send(expectedUser)
    .expect(204));

  afterAll(async () => {
    await app.close();
  });
});
